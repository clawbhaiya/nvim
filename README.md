# nvim

A simple Neovim configuration to get you started right away!

## Installation

```sh
git clone https://gitlab.com/clawbhaiya/nvim.git ~/.config/nvim
```

Open Neovim by `nvim`.
You will see some warnings for the first time. Ignore all the warnings and press <kbd>Enter</kbd> to continue.

Do `:PlugInstall` to install all plugins. After it says "Finishing... Done!",close Neovim with `:qall`.

Relaunch Neovim again to see the configuration in action!
